import javax.smartcardio.Card;
import java.util.Random;

public class Car {

    int vin;
    int stateId;
    int colorId;
    int classificationId;
    int daysToStay;
    int daySinceStay;
    int daysLeft;
    int tips;
    boolean isToBeRemoved;

    @Override
    public String toString() {

        return CarData.getState(stateId) +
                " " + CarData.getColor(colorId) +
                " " + CarData.getClassification(classificationId) + " " + getVin() +
                " стоит тут с " + daySinceStay + " дня. Осталось " + daysLeft + " дня(-ей) до конца стоянки. " +
                "За эту машину вы получите " + daysToStay * 2 + " $ и " + tips + " $ чаевых."
                ;
    }

    public String name() {
        return CarData.getState(stateId) +
                " " + CarData.getColor(colorId) +
                " " + CarData.getClassification(classificationId)
                + " " + getVin();
    }

    public Car() {
    }

    public Car(int daySinceStay) {
        Random random = new Random();
        this.stateId = random.nextInt(CarData.states.length);
        this.colorId = random.nextInt(CarData.colors.length);
        this.classificationId = random.nextInt(CarData.classifications.length);
        this.daysToStay = 1 + random.nextInt(12);
        this.daySinceStay = daySinceStay;
        this.daysLeft = this.daysToStay;
        this.tips = random.nextInt(100);
        this.isToBeRemoved = false;
        this.vin = 1000 + random.nextInt(9000);
    }

    public int getVin() {
        return vin;
    }

    public void setVin(int vin) {
        this.vin = vin;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public int getClassificationId() {
        return classificationId;
    }

    public void setClassificationId(int classificationId) {
        this.classificationId = classificationId;
    }

    public int getDaysToStay() {
        return daysToStay;
    }

    public void setDaysToStay(int daysToStay) {
        this.daysToStay = daysToStay;
    }

    public int getDaySinceStay() {
        return daySinceStay;
    }

    public void setDaySinceStay(int daySinceStay) {
        this.daySinceStay = daySinceStay;
    }

    public int getDaysLeft() {
        return daysLeft;
    }

    public void setDaysLeft(int daysLeft) {
        this.daysLeft = daysLeft;
    }

    public int getTips() {
        return tips;
    }

    public void setTips(int tips) {
        this.tips = tips;
    }

    public boolean isToBeRemoved() {
        return isToBeRemoved;
    }

    public void setToBeRemoved(boolean toBeRemoved) {
        isToBeRemoved = toBeRemoved;
    }
}
