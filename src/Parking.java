import java.util.ArrayList;
import java.util.Random;

public class Parking {

    int maxPlaces;
    int busyPlaces;
    int freePlaces;
    int maxTruckPlaces;
    int busyTruckPlaces;
    int freeTruckPlaces;
    int money;
    int day;
    ArrayList<Integer> income;

    ArrayList<Car> cars;
    ArrayList<Truck> trucks;


    @Override
    public String toString() {
        return "Парковка" +
                " всего вмещает: " + maxPlaces +
                " легковых машин и " + maxTruckPlaces + " грузовых, сейчас свободно: " + freePlaces +
                " мест для легковых и " + freeTruckPlaces + " для грузовых. Баланс парковки: " + money + " $";

    }


    public Parking() {
        this.maxPlaces = 10;
        this.freePlaces = 10;
        this.busyPlaces = 0;
        this.maxTruckPlaces = 5;
        this.freeTruckPlaces = 5;
        this.busyTruckPlaces = 0;
        this.money = 0;
        this.day = 1;
    }

    public int getMaxTruckPlaces() {
        return maxTruckPlaces;
    }

    public void setMaxTruckPlaces(int maxTruckPlaces) {
        this.maxTruckPlaces = maxTruckPlaces;
    }

    public int getBusyTruckPlaces() {
        return busyTruckPlaces;
    }

    public void setBusyTruckPlaces(int busyTruckPlaces) {
        this.busyTruckPlaces = busyTruckPlaces;
    }

    public int getFreeTruckPlaces() {
        return freeTruckPlaces;
    }

    public void setFreeTruckPlaces(int freeTruckPlaces) {
        this.freeTruckPlaces = freeTruckPlaces;
    }

    public int getBusyPlaces() {
        return busyPlaces;
    }

    public void setBusyPlaces(int busyPlaces) {
        this.busyPlaces = busyPlaces;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public ArrayList<Integer> getIncome() {
        return income;
    }

    public void setIncome(ArrayList<Integer> income) {
        this.income = income;
    }

    public ArrayList<Car> getCars() {
        return cars;
    }

    public void setCars(ArrayList<Car> cars) {
        this.cars = cars;
    }

    public ArrayList<Truck> getTrucks() {
        return trucks;
    }

    public void setTrucks(ArrayList<Truck> trucks) {
        this.trucks = trucks;
    }

    public int getMaxPlaces() {
        return maxPlaces;
    }

    public void setMaxPlaces(int maxPlaces) {
        this.maxPlaces = maxPlaces;
    }

    public int getFreePlaces() {
        return freePlaces;
    }

    public void setFreePlaces(int freePlaces) {
        this.freePlaces = freePlaces;
    }

    public void addCars(int count) {
        for (int i = 0; i < count; i++) {
            if (freePlaces > 0) {
                cars.add(new Car(day));
                System.out.println("Приехал: " + cars.get(busyPlaces).name());
                busyPlaces++;
                freePlaces--;
            } else {
                System.out.println("Парковка легковых машин переполнена!");
                System.out.println("Место освободится через " + whenFreeCarSlot() + " дня(-ей)");
            }
        }
    }

    public void addTruck(int count) {
        for (int i = 0; i < count; i++) {
            if (freeTruckPlaces > 0) {
                trucks.add(new Truck(day));
                System.out.println("Приехал: " + trucks.get(busyTruckPlaces).name());
                busyTruckPlaces++;
                freeTruckPlaces--;
            } else {
                System.out.println("Парковка грузовиков переполнена!");
                System.out.println("Место освободится через " + whenFreeTrucksSlot() + " дня(-ей)");
                addTruckToCarParking();
            }
        }
    }

    public void addTruckToCarParking() {
        if (freePlaces > 1) {
            Truck truck = new Truck(day);

            cars.set(busyPlaces, truck);
            busyPlaces++;
            freePlaces--;

            cars.set(busyPlaces, new Truck(day, truck.getDaysToStay()));
            busyPlaces++;
            freePlaces--;

            System.out.println("Приехал: " + truck.name() + " (встал на парковку легковых машин!)");

        } else {
            System.out.println("Парковка легковых машин переполнена!");
            System.out.println("Место освободится через " + whenFreeCarSlot() + " дня(-ей)");
        }

    }

    private int whenFreeCarSlot() {
        int days = cars.get(0).getDaysLeft();
        for (int i = 1; i < busyPlaces - 1; i++) {
            if (days > cars.get(i).getDaysLeft()) {
                days = cars.get(i).getDaysLeft();
            }
        }
        return days;
    }

    private int whenFreeTrucksSlot() {
        int days = trucks.get(0).getDaysLeft();
        for (int i = 1; i < busyTruckPlaces - 1; i++) {
            if (days > trucks.get(i).getDaysLeft()) {
                days = trucks.get(i).getDaysLeft();
            }
        }
        return days;
    }

    public void removeCar(int id) {
        cars.get(id).setToBeRemoved(true);
        if (cars.get(id) instanceof Truck) {
            if (cars.get(id).getVin() == -1) {
                cars.get(id - 1).setToBeRemoved(true);
                cars.remove(id - 1);
                ;
            } else {
                cars.get(id + 1).setToBeRemoved(true);
                cars.remove(id + 1);
            }
        }
        System.out.println("Уехал: " + cars.get(id).name());
        payCars(id);
        cars.remove(id);
        busyPlaces--;
        freePlaces++;
    }

    public void removeTruck(int id) {
        System.out.println("Уехал: " + trucks.get(id).name());
        payTrucks(id);
        trucks.remove(id);
        busyTruckPlaces--;
        freeTruckPlaces++;
    }

    //Получение денег за стоянку машины
    private void payCars(int id) {
        int moneyForCar = (cars.get(id).getDaysToStay() - cars.get(id).getDaysLeft()) * 2;
        if (cars.get(id).getDaysLeft() == 0) {
            moneyForCar += cars.get(id).getTips();
        }
        System.out.println("Вы заработали " + moneyForCar + " $");
        money += moneyForCar;
    }

    private void payTrucks(int id) {
        int moneyForTruck = (trucks.get(id).getDaysToStay() - trucks.get(id).getDaysLeft()) * 4;
        if (trucks.get(id).getDaysLeft() == 0) {
            moneyForTruck += trucks.get(id).getTips();
        }
        System.out.println("Вы заработали " + moneyForTruck + " $");
        money += moneyForTruck;
    }

    //Проверка истечения срока стоянки
    public void checkParkingEnd() {
        for (int i = 0; i < busyPlaces; i++) {
            cars.get(i).setDaysLeft(cars.get(i).getDaysLeft() - 1);
            if (cars.get(i).getDaysLeft() <= 0) {
                removeCar(i);
            }
        }
        for (int i = 0; i < busyTruckPlaces; i++) {
            trucks.get(i).setDaysLeft(trucks.get(i).getDaysLeft() - 1);
            if (trucks.get(i).getDaysLeft() <= 0) {
                removeTruck(i);
            }
        }
    }

    //Прибытие новых машин по началу нового дня
    public void arrivalEvent() {
        Random random = new Random();
        addCars(random.nextInt(freePlaces / 2));
        addTruck(random.nextInt(freeTruckPlaces / 3));
    }


}
