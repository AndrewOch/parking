public class CarData {
    static String[] states = {"Новый", "Чистый", "Сломанный", "Разбитый", "Грязный", "Старый", "Пыльный", "Треснувший", "Угнанный", "Поцарапанный", "Подаренный", "Шумный", "Крашенный",
            "БУ", "Только купленный"};
    static String[] classifications = {"лимузин", "ландо", "фаэтон", "пульман-лимузин", "минивэн", "седан", "кабриолет", "седан-хардтоп",
            "универсал", "хэтчбек", "купе", "купе-кабриолет", "родстер", "купе-тарга", "пикап",};
    static String[] colors = {"красный", "жёлтый", "синий", "белый", "чёрный", "рыжий", "бурый", "фиолетовый", "радужный", "серый", "золотой", "серебристый", "бежевый", "сизый", "бордовый", "зелёный"};


    public CarData() {
    }

    public static String getClassification(int id) {
        return classifications[id];
    }

    public static String getState(int id) {
        return states[id];
    }

    public static String getColor(int id) {
        return colors[id];
    }


}
