import java.util.Random;

public class Truck extends Car {

    public Truck() {
    }

    public Truck(int daySinceStay,int dayToStay) {
        //Конструктор объекта-дополнительного слота при постановке на парковку легковых машин
        this.daysToStay = dayToStay;
        this.daySinceStay = daySinceStay;
        this.daysLeft = this.daysToStay;
        this.isToBeRemoved = false;
        this.vin = -1;
    }

    public Truck(int daySinceStay) {
        Random random = new Random();
        this.stateId = random.nextInt(TruckData.states.length);
        this.colorId = random.nextInt(TruckData.colors.length);
        this.classificationId = random.nextInt(TruckData.classifications.length);
        this.daysToStay = 3 + random.nextInt(15);
        this.daySinceStay = daySinceStay;
        this.daysLeft = this.daysToStay;
        this.tips = random.nextInt(300);
        this.isToBeRemoved = false;
        this.vin = 1000 + random.nextInt(9000);
    }

    @Override
    public String toString() {

        return TruckData.getState(stateId) +
                " " + TruckData.getColor(colorId) +
                " " + TruckData.getClassification(classificationId) + " " + getVin() +
                " стоит тут с " + daySinceStay + " дня. Осталось " + daysLeft + " дня(-ей) до конца стоянки. " +
                "За эту машину вы получите " + daysToStay * 4 + " $ и " + tips + " $ чаевых."
                ;
    }

    public String name() {
        return TruckData.getState(stateId) +
                " " + TruckData.getColor(colorId) +
                " " + TruckData.getClassification(classificationId)
                + " " + getVin();
    }
}
