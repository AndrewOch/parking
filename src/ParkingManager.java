
import java.util.ArrayList;
import java.util.Scanner;

public class ParkingManager {


    static Parking parking = new Parking();


    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Сколько мест вмещает парковка?");

        int places = scanner.nextInt();

        System.out.println("Сколько грузовых мест вмещает парковка?");

        int truckPlaces = scanner.nextInt();

        parking.setMaxPlaces(places);
        parking.setFreePlaces(places);

        parking.setMaxTruckPlaces(truckPlaces);
        parking.setFreeTruckPlaces(truckPlaces);

        parking.cars = new ArrayList<>(0);
        parking.trucks = new ArrayList<>(0);

        System.out.println("/help , чтобы посмотреть все команды");
        controlPanel();
    }


    private static void help() {
        System.out.println("/help - список команд\n\n" +
                "Время:\n" +
                "/next - следующий день\n" +
                "/skip - пропустить несколько дней\n\n" +

                "Машины:\n" +
                "/cars - проверить машины на стоянке\n" +
                "/add car - добавить машины на парковку\n" +
                "/remove car - убрать машины с парковки\n\n" +

                "Грузовики:\n" +
                "/trucks - проверить грузовики на стоянке\n" +
                "/add truck - добавить грузовики на парковку\n" +
                "/remove truck - убрать грузовики с парковки\n\n" +

                "Парковка:\n" + "/info - посмотреть информацию о парковке\n" +
                "/money - узнать баланс стоянки\n" +
                "/build c - построить новые парковочные места для легковых машин\n" +
                "/build t - построить новые парковочные места для грузовых машин\n" +
                "/income - посмотреть финансовый отчёт\n\n" +

                "/exit - завершить работу\n");
    }

    private static void controlPanel() throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("День " + parking.day + " |:");
        String command = scanner.nextLine();
        findCommand(command);
    }

    private static void findCommand(String command) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        if (command.equals("/help")) {
            help();
        }

        if (command.equals("/exit")) {
            return;
        }
        if (command.equals("/money")) {
            System.out.println("Баланс: " + parking.getMoney() + "$");
        }
        if (command.equals("/info")) {
            System.out.println(parking.toString());
        }
        if (command.equals("/build c")) {
            buildC();
        }
        if (command.equals("/build t")) {
            buildT();
        }
        if (command.equals("/skip")) {
            System.out.println("Сколько дней пропустить?");
            int days = scanner.nextInt();
            skip(days);
        }
        if (command.equals("/next")) {
            skip(1);
        }
        if (command.equals("/add car")) {
            System.out.println("Введите, сколько машин вы хотите добавить:");
            int count = scanner.nextInt();
            parking.addCars(count);
        }
        if (command.equals("/remove car")) {
            showCars();
            System.out.println("Введите id машины, которую хотите убрать с парковки. Внимание, вы не получите чаевые!");
            int id = scanner.nextInt();
            parking.removeCar(id);
        }
        if (command.equals("/add truck")) {
            System.out.println("Введите, сколько грузовиков вы хотите добавить:");
            int count = scanner.nextInt();
            parking.addTruck(count);
        }
        if (command.equals("/remove truck")) {
            showTrucks();
            System.out.println("Введите id грузовика, который хотите убрать с парковки. Внимание, вы не получите чаевые!");
            int id = scanner.nextInt();
            parking.removeTruck(id);
        }
        if (command.equals("/cars")) {
            showCars();
        }
        if (command.equals("/trucks")) {
            showTrucks();

        }
        if (command.equals("/income")) {
            System.out.println("Вскоре можно будет смотреть финансовый отчёт... Функция пока не готова.");
        }
        controlPanel();
    }

    private static void showTrucks() {
        if (parking.getBusyTruckPlaces() > 0) {
            for (int i = 0; i < parking.trucks.size(); i++) {
                System.out.println(i + ". " + parking.trucks.get(i).toString());
            }
        } else System.out.println("На парковке пусто!");
    }

    private static void showCars() {

        if (parking.getBusyPlaces() > 0) {
            for (int i = 0; i < parking.cars.size(); i++) {

                if (parking.cars.get(i) instanceof Truck) {
                    System.out.print(i + "-");
                    i++;
                    System.out.println(i + ". " + parking.cars.get(i - 1).toString());
                } else {
                    System.out.println(i + ". " + parking.cars.get(i).toString());
                }
            }
        } else System.out.println("На парковке пусто!");
    }

    private static void skip(int days) {
        if (days > 0) {
            for (int i = 0; i < days; i++) {
                parking.setDay(parking.getDay() + 1);
                parking.checkParkingEnd();
                parking.arrivalEvent();
            }
        } else System.out.println("Вы должны ввести положительное число!");
    }

    private static void buildC() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Каждое парковочное место будет стоить 100$");
        System.out.println("Введите, сколько парковочных мест добавить: ");
        int count = scanner.nextInt();
        if (parking.getMoney() < count * 100) {
            System.out.println("Вам не хватает " + (count * 100 - parking.getMoney()) + " на такую покупку!");
            return;
        }
        parking.setMaxPlaces(parking.getMaxPlaces() + count);
        parking.setFreePlaces(parking.getMaxPlaces() - parking.getBusyPlaces());
        parking.setMoney(parking.getMoney() - count * 100);
        System.out.println("Построено " + count + " новых парковочных мест для легковых машин!");
        System.out.println("Баланс: " + parking.getMoney() + "$");
    }

    private static void buildT() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Каждое парковочное место будет стоить 200$");
        System.out.println("Введите, сколько парковочных мест добавить: ");
        int count = scanner.nextInt();
        if (parking.getMoney() < count * 200) {
            System.out.println("Вам не хватает " + (count * 200 - parking.getMoney()) + " на такую покупку!");
            return;
        }
        parking.setMaxTruckPlaces(parking.getMaxTruckPlaces() + count);
        parking.setFreeTruckPlaces(parking.getMaxTruckPlaces() - parking.getBusyTruckPlaces());
        parking.setMoney(parking.getMoney() - count * 200);
        System.out.println("Построено " + count + " новых парковочных мест для грузовых машин!");
        System.out.println("Баланс: " + parking.getMoney() + "$");
    }
}