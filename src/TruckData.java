public class TruckData {

    static String[] states = {"Новый", "Чистый", "Сломанный", "Разбитый", "Грязный", "Старый", "Пыльный", "Треснувший", "Угнанный", "Поцарапанный", "Подаренный", "Шумный", "Крашенный",
            "БУ", "Только купленный"};
    static String[] classifications = {"грузовик", "камаз","эвакуатор","мусоровоз", "автозак", "бигфут","фургон","пожарный фургон","автобус","танк","автокран","троллейбус"};
    static String[] colors = {"красный", "жёлтый", "синий", "белый", "чёрный", "рыжий", "бурый", "фиолетовый", "радужный", "серый", "золотой", "серебристый", "бежевый", "сизый", "бордовый", "зелёный"};


    public TruckData() {
    }

    public static String getClassification(int id) {
        return classifications[id];
    }

    public static String getState(int id) {
        return states[id];
    }

    public static String getColor(int id) {
        return colors[id];
    }


}
